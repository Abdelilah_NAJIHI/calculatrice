package calculatrice;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Classe implantant une pile de nombres. On peut empiler des nombres, dépiler
 * <i>n</i> nombres d'un coup, et afficher la pile.
 */
public class Calculatrice {
  private Deque<Double> pile;

  public Calculatrice() {
    pile = new ArrayDeque<>();
  }

  public void empiler(double valeur) {
    pile.offerLast(valeur);
  }

  public double[] depiler(int nbValeurs) {
    double[] retour = new double[nbValeurs];
    for (int i = 0; i < nbValeurs; i++) {
      retour[i] = pile.pollLast();
    }
    return retour;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    List<Double> contenu = new ArrayList<>(pile);
    for (int i = contenu.size(); i > 0; i--) {
      double valeur = contenu.get(i - 1);
      String repr;
      if ((int) valeur == valeur) {
        repr = String.format("%4d:%15d\n", i, (int) valeur);
      } else {
        repr = String.format("%4d:%15g\n", i, valeur);
      }
      builder.append(repr);
    }
    return builder.toString();
  }
}
