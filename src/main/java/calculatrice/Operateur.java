package calculatrice;

public enum Operateur {
  PLUS(2){	  
    public double calc(double... operandes) {
      return operandes[0] + operandes[1];
    }
  },
  MULTI(2){
		@Override
		public double calc(double... operandes) {
			return operandes[0] * operandes[1];
    }
  },
  MODULO(2){
    @Override
		public double calc(double... operandes) {
			return operandes[0] % operandes[1];
    }
  },
  INVERSE(1){
    @Override
		public double calc(double... operandes) {
			return operandes[0] / operandes[1];
	}

},
    PUISS(2){
      @Override
      public double calc(double... operandes) {
        return operandes[1] * operandes[1];
      }		
    },
    SOUSTRACTION(2){
      @Override
      public double calc(double... operandes) {
        return operandes[0] - operandes[1];
      }		
    },
  
    
      Div(4){
      public double calc(double... operandes) {
        return operandes[0] / operandes[1];
        }
      },
      SINUS(2) {        
		@Override
		public double calc(double... operandes) {
			double R=0.5*0.5,S=42.0;
	        for(byte I=10;
	        I>=1;I--) S=4.0*I-2.0+(-R)/S;
	        return 2.0*0.5*S/(R+S*S);
		}

   },
  TAN(2){
	   @Override
	   public double calc(double... operandes)
	   {
		   return Math.tanh(operandes[0] + operandes[1]);
	   }
   },
  COS(2){
	   @Override
	   public double calc(double... operandes){
			  return Math.cosh(operandes[0] + operandes[1]);
			  }
   },
  EXP(1){
	@Override
	public double calc(double... operandes) {
		return Math.exp(operandes[0]);
	}	   
   };

  private int arite;

  Operateur(int monArite) {
    arite = monArite;
  }

  public abstract double calc(double... operandes);

  public int getArite() {
    return arite;
  }
}
