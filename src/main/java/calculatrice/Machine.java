package calculatrice;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe implantant une machine à calculer fonctionnant en notation polonaise
 * inversée.
 */
public class Machine {
  private Calculatrice calc;
  private Map<String, Operateur> operateurs;

  public Machine() {
    calc = new Calculatrice();
    operateurs = new HashMap<>();
    operateurs.put("+", Operateur.PLUS);
    operateurs.put("*", Operateur.MULTI);
    operateurs.put("%", Operateur.MODULO);
    operateurs.put("'\'", Operateur.INVERSE);
    operateurs.put("-", Operateur.SOUSTRACTION);
    operateurs.put("sin", Operateur.SINUS);
    operateurs.put("tan", Operateur.TAN);
    operateurs.put("cos", Operateur.COS);
    operateurs.put("e", Operateur.EXP);
  }

  public void evaluer(String chaine) {
    String[] tokens = chaine.split(" ");
    for (String token : tokens) {
      try {
        double valeur = Double.parseDouble(token);
        calc.empiler(valeur);
      } catch (NumberFormatException nfe) {
        traiterOperateur(token);
      }
    }
  }

  public void traiterOperateur(String chaine) {
    Operateur op = operateurs.get(chaine);
    double[] operandes = calc.depiler(op.getArite());
    double resultat = op.calc(operandes);
    calc.empiler(resultat);
  }

  @Override
  public String toString() {
    return calc.toString();
  }
}
