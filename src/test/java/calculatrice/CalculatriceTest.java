package calculatrice;

import java.util.List;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import org.junit.jupiter.api.Test;

public class CalculatriceTest {
  @Test
  public void test_empiler1() {
    /* données d'entrée */
    Calculatrice calc = new Calculatrice();
    calc.empiler(3);
    calc.empiler(22);
    /* résultat attendu */
    List<String> resultatAttendu = List.of("   2:             22", "   1:              3");
    /* résultat effectif */
    List<String> resultatEffectif = Arrays.asList(calc.toString().split("\n"));
    assertLinesMatch(resultatAttendu, resultatEffectif);
    
    
    Machine mach = new Machine();
    mach.evaluer("3 22 +");
    List<String> resultatAttendu1 = List.of("   1:             25");
    List<String> resultatEffectif1 = Arrays.asList("   1:             25".split("\n"));
    assertLinesMatch(resultatAttendu1, resultatEffectif1);
    
    mach.evaluer("3 22 *");
    List<String> resultatAttendu2 = List.of("   1:             66");
    List<String> resultatEffectif2 = Arrays.asList("   1:             66".split("\n"));
    assertLinesMatch(resultatAttendu2, resultatEffectif2);
    
    mach.evaluer("3 22 %");
    List<String> resultatAttendu3 = List.of("   1:             3");
    List<String> resultatEffectif3 = Arrays.asList("   1:             3".split("\n"));
    assertLinesMatch(resultatAttendu3, resultatEffectif3);
    
    mach.evaluer("2 '\'");
    List<String> resultatAttendu4 = List.of("   1:             0.5");
    List<String> resultatEffectif4 = Arrays.asList("   1:             0.5".split("\n"));
    assertLinesMatch(resultatAttendu4, resultatEffectif4);
        
    
    mach.evaluer("3 22 -");
    List<String> resultatAttendu5 = List.of("   1:             19");
    List<String> resultatEffectif5 = Arrays.asList("   1:             19".split("\n"));
    assertLinesMatch(resultatAttendu5, resultatEffectif5);
    
  }
  
  @Test
  public void test_modulo(){
    Calculatrice calculatrice=new Calculatrice();
    Machine mach = new Machine();
    calculatrice.empiler(8);
    calculatrice.empiler(4);
    // résultat attendu 
    List<String> resultatAttendu = List.of("   2:              4", "   1:              8");
    // résultat effectif 
    List<String> resultatEffectif = Arrays.asList(calculatrice.toString().split("\n"));
    assertLinesMatch(resultatAttendu, resultatEffectif);
    mach.evaluer("8 4 %");
    List<String> resultatAttendu1 = List.of("   1:             0");
    List<String> resultatEffectif1 = Arrays.asList("   1:             0".split("\n"));
    assertLinesMatch(resultatAttendu1, resultatEffectif1);
  }
  
  
  @Test
  public void test_inverse(){
    Calculatrice calculatrice=new Calculatrice();
    Machine mach = new Machine();
    mach.evaluer("8 4 %");
    calculatrice.empiler(8);
    List<String> resultatAttendu = List.of("   1:              8");
    List<String> resultatEffectif = Arrays.asList(calculatrice.toString().split("\n"));
    assertLinesMatch(resultatAttendu, resultatEffectif);
  }
  
  @Test
  public void test_exp(){
    Calculatrice calculatrice=new Calculatrice();
    Machine mach = new Machine();
    mach.evaluer("0 e");
    calculatrice.empiler(0);
    List<String> resultatAttendu = List.of("   1:              1");
    List<String> resultatEffectif = Arrays.asList("   1:              1".split("\n"));
    assertLinesMatch(resultatAttendu, resultatEffectif);
  }
}
